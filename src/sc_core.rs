elrond_wasm::imports!();
elrond_wasm::derive_imports!();

#[elrond_wasm::module]
pub trait CoreModule: crate::storage::StorageModule {
    fn core_init(&self) {
        self.paused().set_if_empty(&false);
    }

    #[view(isPaused)]
    fn is_paused(&self) -> bool {
        return self.paused().get();
    }

    #[only_owner]
    #[endpoint]
    fn pause_sc(&self) {
        self.paused().set(&true);
    }

    #[only_owner]
    #[endpoint]
    fn resume_sc(&self) {
        self.paused().set(&false);
    }
}
