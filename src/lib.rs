#![no_std]

elrond_wasm::imports!();
elrond_wasm::derive_imports!();

mod storage;
mod sc_core;

#[elrond_wasm::contract]
pub trait CoreEmptyContract:
    storage::StorageModule
    + sc_core::CoreModule {
    #[init]
    fn init(&self) {
        self.core_init();
    }
}
