elrond_wasm::imports!();
elrond_wasm::derive_imports!();

#[elrond_wasm::module]
pub trait StorageModule {
    #[storage_mapper("paused")]
    fn paused(&self) -> SingleValueMapper<bool>;

    #[storage_mapper("user")]
	fn user_mapper(&self) -> UserMapper;

    #[view(userId)]
    fn user_id(&self, address: ManagedAddress) -> usize {
        return self.user_mapper().get_user_id(&address);
    }

    #[view(userAddress)]
    fn user_address(&self, user_id: usize) -> Option<ManagedAddress> {
        return self.user_mapper().get_user_address(user_id);
    }
}

