////////////////////////////////////////////////////
////////////////// AUTO-GENERATED //////////////////
////////////////////////////////////////////////////

#![no_std]

elrond_wasm_node::wasm_endpoints! {
    core_empty_contract
    (
        isPaused
        pause_sc
        resume_sc
        userAddress
        userId
    )
}

elrond_wasm_node::wasm_empty_callback! {}
